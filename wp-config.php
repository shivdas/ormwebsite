<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'orm_website');

/** MySQL database username */
define('DB_USER', 'orm_wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'jdhfuu#23@#dfd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lIWSq!.]^Y)h`tkfM]U=(rQK$#+zwG=W%wB8gVv0IS?(`$69Kitqa~85RIp6@{tp');
define('SECURE_AUTH_KEY',  'PZ`:B4I=+cPJ@I{M[-W |m@ImnE#{Qrwf{|Al)]rW#D~JV=0Zcq1KWLfRi|GGLKz');
define('LOGGED_IN_KEY',    '..m.j{^_VNQ7 !j#{o/wOcKOEXEKReK:71 lYM~Uq#R@v^cBHx7-C2D;NYiS];[5');
define('NONCE_KEY',        '`u@^-SvV~& ~=zbu%t_4kv:;FidS?g.)mCa]Fucnp4C2l?N?rC63<7EI~n}4$:F.');
define('AUTH_SALT',        'A|g!T.sJTOzF_k(..~e7SlHD[6<U1X2B_?@ZL+:%ILHz2O^/{}6|NdQILEa<>c9V');
define('SECURE_AUTH_SALT', 'AlJC*d)Zb9p84l<J5K+h~a.ZUHetu|/ bGf+cr+EtM/U1vjGAvQFil v)VpFn|#1');
define('LOGGED_IN_SALT',   '3+:T4v]8nmPy]`HyGF*!cHM{g!a0lCgTotRLnzHSOKh+/{AoRpkukkquFDsP!y{9');
define('NONCE_SALT',       'A%rOb4WuWP+j:J*H|<Ethi|s,/Fe%}k%)+Q/F`8(5G*Bhi-r,)asbE%p7kPMb$e2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
